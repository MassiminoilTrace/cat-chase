#define ATTINY

#ifdef ATTINY
#define PIN_LEFT PB3
#define PIN_RIGHT PB4
#define PIN_BUZZER PB1
#else
#define PIN_LEFT 6
#define PIN_RIGHT 5
#define PIN_BUZZER 13
#endif

#include "i2c_wrapper.h"
#include "ssd1306_minimal.h"
#include "immagini.h"
#include "gamestate.h"
#include "score.h"
#include "tone.h"

#define CAT_SPEED 7
#define AIM_TARGET_SPEED 10


#define Y_POS_LOWER_ROW SCREEN_HEIGHT - SPRITE_EDGE
#define NUMBER_OF_ENEMIES 2




static enum GameState cur_game_state = GameStateTitleScreen;	//GameStateScore;

struct Enemy
{
  unsigned char x_origin;
  unsigned char y_origin;
  char vel;			// Per ora non utilizzato
};


struct Enemy enemies[NUMBER_OF_ENEMIES];
bool enemies_alive[NUMBER_OF_ENEMIES] = { 0 };

bool enemy_should_recreate = false;

struct Sprite
{
  unsigned char x_origin;
  unsigned char y_origin;
};
#include "enemy.h"

struct Sprite cat_sprite = Sprite {.x_origin = 0,.y_origin = Y_POS_LOWER_ROW
};

struct Sprite aiming_target = Sprite {.x_origin = 0,.y_origin = SCREEN_HEIGHT
};

// Prototipi
void draw_sections ();
void draw_title_screen ();
void update_objects ();
void start_new_game ();
void enemy_set_random (struct Enemy *enemy, bool screen_side);

void draw_block (byte img_ram[],
		 unsigned char origin_x,
		 unsigned char origin_y,
		 unsigned char section_number, bool flipped)
{
  // Legge da img_ram
  const unsigned char y_section_low = section_number * (SCREEN_HEIGHT / PIXEL_NUMBER_DIVISOR);	// Incluso
  const unsigned char y_section_high = (section_number + 1) * (SCREEN_HEIGHT / PIXEL_NUMBER_DIVISOR);	// Escluso

  for (unsigned char local_x = 0; local_x < SPRITE_EDGE; local_x++)
  {
    for (unsigned char local_y = 0; local_y < SPRITE_EDGE; local_y++)
    {
      // Coordinate globali, senza considerare le sezioni
      unsigned char framebuf_x;
      if (flipped)
      {
	framebuf_x = origin_x - local_x + SPRITE_EDGE;
      }
      else
      {
	framebuf_x = origin_x + local_x;
      }
      unsigned char framebuf_y = origin_y + local_y;

      // Salto il pixel se sono fuori dai confini
      if ((framebuf_x < 0 || framebuf_x >= SCREEN_WIDTH)
	  ||
	  (framebuf_y < y_section_low || framebuf_y > y_section_high)
	  || (framebuf_y >= SCREEN_HEIGHT))
      {
	continue;
      }


      // Se è acceso, disegno il pixel
      // Sottraggo la y_section_limit_low alla coordinata y prima di disegnarla
      if (sprite_get_at (img_ram, local_x, local_y))
      {
	SSD1306_MINIMAL_setPixel (framebuf_x, framebuf_y - y_section_low);
      }
    }
  }
}

void setup ()
{

  pinMode (PIN_LEFT, INPUT_PULLUP);
  pinMode (PIN_RIGHT, INPUT_PULLUP);
  pinMode (PIN_BUZZER, OUTPUT);


  //delay(20);


  I2C_WRAPPER_init ();
  delay (30);
  SSD1306_MINIMAL_init ();
  delay (30);

  tone_setup ();

  //start_new_game();
}

void loop ()
{
  InputCommand cmd;
  {
    bool left_press = digitalRead (PIN_LEFT) == LOW;
    bool right_press = digitalRead (PIN_RIGHT) == LOW;
    if (left_press && right_press)
    {
      cmd = InputJump;
    }
    else if (left_press)
    {
      cmd = InputLeft;
    }
    else if (right_press)
    {
      cmd = InputRight;
    }
    else
    {
      cmd = InputNone;
    }
  }

  if (cur_game_state == GameStateTitleScreen && cmd != InputNone)
  {
    start_new_game ();
  }


  if (cur_game_state == GameStateTitleScreen)
  {
    draw_title_screen ();
    return;
  }

  if (cur_game_state == GameStateScore)
  {
    if (cmd != InputNone)
    {
      cur_game_state = GameStateTitleScreen;
      draw_title_screen ();
      delay (1000);
    }
    return;
  }

  if (is_game_finished ())
  {
    tone_start (500);
    cur_game_state = GameStateScore;
    draw_score ();
    delay (1000);
    // Disegno il punteggio solo una volta.
    // poiché viene azzerato per essere disegnato.
    // Dopodiché, rimango in loop fino a pressione tasto
    return;
  }

  /* Move objects */
  update_objects (cmd);


  /* Transfer framebuffer to device */
  draw_sections ();
}


void update_objects (InputCommand cmd)
{
  if (cur_game_state == GameStateTitleScreen
      || cur_game_state == GameStateScore)
  {
    return;
  }

  switch (cur_game_state)
  {
  case GameStateAiming:
    // Non posso dare comandi al gatto,
    // continuo normalmente.
    // Posso solo continuare o smettere di mirare
    if (cmd == InputJump)
    {
      // continuo a far salire il cursore
      aiming_target.y_origin -= AIM_TARGET_SPEED;
      if (aiming_target.y_origin > 200)
      {
	aiming_target.y_origin = 0;
      }
    }
    else
    {
      cur_game_state = GameStateCatJump;
    }
    break;
  case GameStateIdle:
    // Posso muovermi o mirare il salto

    //  Devo ricreare dei nuovi topini?
    if (enemy_should_recreate)
    {
      for (unsigned char i = 0; i < NUMBER_OF_ENEMIES; i++)
      {
	if (!enemies_alive[i])
	{
	  enemy_set_random (&enemies[i], true);
	  enemies_alive[i] = true;
	}
      }
      enemy_should_recreate = false;
    }

    // Sposto o salto
    if (cmd == InputRight)
    {
      cat_sprite.x_origin = constrain (cat_sprite.x_origin + CAT_SPEED,
				       0, SCREEN_WIDTH - SPRITE_EDGE);
    }
    else if (cmd == InputLeft)
    {
      cat_sprite.x_origin = cat_sprite.x_origin - CAT_SPEED;
      // Se overflow in giù
      if (cat_sprite.x_origin > 150)
      {
	cat_sprite.x_origin = 0;
      }
    }
    else if (cmd == InputJump)
    {
      cur_game_state = GameStateAiming;
      aiming_target.x_origin = cat_sprite.x_origin;
      aiming_target.y_origin = Y_POS_LOWER_ROW;
    }
    break;
  case GameStateCatJump:
    // Non posso dare altri comandi
    // Continuo a spostare in su il gatto
    // fino a raggiungere origine del aiming_target
    cat_sprite.y_origin = (cat_sprite.y_origin + aiming_target.y_origin) / 2;
    if (cat_sprite.y_origin == aiming_target.y_origin)
    {
      // Sovrapposti. FIne salto.

      // Verifico se ho colpito qualcosa
      for (unsigned char i = 0; i < NUMBER_OF_ENEMIES; i++)
      {
	if (!enemies_alive[i])
	{
	  continue;
	}
	if (check_collision (&cat_sprite, &enemies[i]))
	{
	  tone_start (50);
	  enemies_alive[i] = false;
	  enemy_should_recreate = true;
	  add_score ();
	}
      }

      // Resetto gatto in basso
      cat_sprite.y_origin = Y_POS_LOWER_ROW;
      aiming_target.y_origin = SCREEN_HEIGHT;
      cur_game_state = GameStateIdle;
    }
    break;
  }
  // Move enemies
  for (unsigned char i = 0; i < NUMBER_OF_ENEMIES; i++)
  {
    enemies[i].x_origin += enemies[i].vel;

    // Check if an enemy is out of the screen.
    // Teleport it on the other side at a random height
    if ((enemies[i].x_origin < 255 - SPRITE_EDGE)
	&& (enemies[i].x_origin > SCREEN_WIDTH))
    {
      enemy_set_random (&enemies[i], true);
    }
  }
}


// Disegna la sezione indicata, num da 0 a 3
//   _
// | 0 |
// | - |
// | 1 |
// | - |
// | 2 |
// | - |
// | 3 |
//   -
void draw_sections ()
{
  // For every section
  for (unsigned char current_section = 0;
       current_section < PIXEL_NUMBER_DIVISOR; current_section++)
  {
    /* Clear the framebuffer */
    for (int i = 0; i < PIXEL_NUMBER_SECTION; i++)
    {
      SSD1306_MINIMAL_framebuffer[i] = 0;
    }

    // Carico nemico in img_ram
    // --
    // Disegno i nemici

    byte img_ram[SPRITE_BYTES];
    // Temporaneo uso solo topo
    memcpy_P (img_ram, topo, SPRITE_BYTES);
    for (unsigned char i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
      if (enemies_alive[i])
      {
	draw_block (img_ram,
		    enemies[i].x_origin,
		    enemies[i].y_origin, current_section, enemies[i].vel > 0);
      }
    }

    // Disegno gatto
    if (cur_game_state == GameStateCatJump)
    {
      memcpy_P (img_ram, gatto_salta, SPRITE_BYTES);
    }
    else if (cur_game_state == GameStateAiming)
    {
      memcpy_P (img_ram, gatto_carica_salto, SPRITE_BYTES);
    }
    else
    {
      memcpy_P (img_ram, gatto, SPRITE_BYTES);
    }
    draw_block (img_ram,
		cat_sprite.x_origin,
		cat_sprite.y_origin, current_section, false);



    // Disegno mirino, se in aiming
    memcpy_P (img_ram, mirino, SPRITE_BYTES);
    draw_block (img_ram,
		aiming_target.x_origin,
		aiming_target.y_origin, current_section, false);




    // Transfer the section to the screen
    SSD1306_MINIMAL_transferFramebuffer ();
  }

}

void start_new_game ()
{
  cur_game_state = GameStateIdle;
  randomSeed (millis ());
  for (unsigned char i = 0; i < NUMBER_OF_ENEMIES; i++)
  {
    enemy_set_random (&enemies[i], false);
    enemies_alive[i] = true;
  }

  cat_sprite.x_origin = 0;
  cat_sprite.y_origin = Y_POS_LOWER_ROW;

  aiming_target.y_origin = SCREEN_HEIGHT;
  start_game_timer ();
  reset_score ();
}


void enemy_set_random (struct Enemy *enemy, bool screen_side)
{
  bool is_vel_positive = random (0, 2);
  if (screen_side)
  {
    if (is_vel_positive)
    {
      enemy->x_origin = 0 - SPRITE_EDGE;
    }
    else
    {
      enemy->x_origin = SCREEN_WIDTH;
    }
  }
  else
  {
    enemy->x_origin = random (0, SCREEN_WIDTH - SPRITE_EDGE);
  }
  enemy->y_origin =
    random (0, Y_POS_LOWER_ROW - SPRITE_EDGE - SPRITE_EDGE / 2);

  if (is_vel_positive)
  {
    enemy->vel = random (2, 6);
  }
  else
  {
    enemy->vel = -random (2, 6);
  }
}

void draw_title_screen ()
{
  byte img_ram[16];
  for (int section = 0; section < PIXEL_NUMBER_DIVISOR; section++)
  {
    for (int i = 0; i < PIXEL_NUMBER_SECTION; i++)
    {
      SSD1306_MINIMAL_framebuffer[i] = 0;
    }
    for (unsigned char y = 0; y < SCREEN_HEIGHT / PIXEL_NUMBER_DIVISOR; y++)
    {
      memcpy_P (img_ram,
		&title_screen[section * PIXEL_NUMBER_SECTION + y * 16], 16);
      for (unsigned char x = 0; x < SCREEN_WIDTH; x++)
      {
	if (titlescreen_get_at (img_ram, x, 0))
	{
	  SSD1306_MINIMAL_setPixel (x, y);
	}
      }
    }
    SSD1306_MINIMAL_transferFramebuffer ();
  }
}
