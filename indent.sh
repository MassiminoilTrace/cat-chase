#!/bin/sh
# Use GNU Indent to format code style

indent -bl -bli0 -npsl cat-chase.ino
indent -bl -bli0 -npsl enemy.h
indent -bl -bli0 -npsl i2c_wrapper.h
indent -bl -bli0 -npsl score.h
indent -bl -bli0 -npsl gamestate.h
indent -bl -bli0 -npsl immagini.h
indent -bl -bli0 -npsl ssd1306_minimal.h
indent -bl -bli0 -npsl tone.h
