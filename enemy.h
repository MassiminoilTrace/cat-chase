#define MAX_X_COLLISION_DETECTION 8
#define MAX_Y_COLLISION_DETECTION 8

bool check_collision (Sprite *cat, Enemy *enemy)
{
  unsigned char delta_x;
  {
    if (cat->x_origin > enemy->x_origin)
    {
      delta_x = cat->x_origin - enemy->x_origin;
    }
    else
    {
      delta_x = enemy->x_origin - cat->x_origin;
    }
  }
  if (delta_x > MAX_X_COLLISION_DETECTION)
  {
    return false;
  }

  unsigned char delta_y;
  {
    if (cat->y_origin > enemy->y_origin)
    {
      delta_y = cat->y_origin - enemy->y_origin;
    }
    else
    {
      delta_y = enemy->y_origin - cat->y_origin;
    }
  }
  if (delta_y > MAX_Y_COLLISION_DETECTION)
  {
    return false;
  }
  return true;
}
