enum GameState
{
  // Schermata fissa
  GameStateTitleScreen = 0,
  // Gatto fermo, il mirino sale fino a quando lascio i pulsanti
  GameStateAiming = 1,
  // Non sto saltando, posso muovermi a destra e sinistra e dare il comando di mira. 
  // Si mira premendo contemporaneamente i due pulsanti
  GameStateIdle = 2,
  // Il gatto sta saltando, non fare nulla a parte aspettare che finisca l'animazione
  GameStateCatJump = 3,
  // Schermata punteggio finale
  GameStateScore = 4
};

/*
 * In TitleScreen la pressione di un qualsiasi
 * pulsante fa iniziare la partita azzerando il punteggio.
 * 
 * In game state aiming la croce viene disegnata per ultima
 * 
 * 
 * 
 */



enum InputCommand
{
  InputNone,
  InputLeft,
  InputRight,
  InputJump
};
