#define MATCH_DURATION_MS 45000
static unsigned int score = 123;

// At what millis will the game end?
static unsigned long finish_millis = 0;

void reset_score ()
{
  score = 0;
}

void add_score ()
{
  score += 1;
}


// Writes the last digit to the provided array
// 
void draw_score (byte img_ram[8])
{
  unsigned char digit = score % 10;
  score = score / 10;
  switch (digit)
  {
  case 0:
    memcpy_P (img_ram, digit_0, 8);
    break;
  case 1:
    memcpy_P (img_ram, digit_1, 8);
    break;
  case 2:
    memcpy_P (img_ram, digit_2, 8);
    break;
  case 3:
    memcpy_P (img_ram, digit_3, 8);
    break;
  case 4:
    memcpy_P (img_ram, digit_4, 8);
    break;
  case 5:
    memcpy_P (img_ram, digit_5, 8);
    break;
  case 6:
    memcpy_P (img_ram, digit_6, 8);
    break;
  case 7:
    memcpy_P (img_ram, digit_7, 8);
    break;
  case 8:
    memcpy_P (img_ram, digit_8, 8);
    break;
  case 9:
    memcpy_P (img_ram, digit_9, 8);
    break;
  }
}

// Time tracking
void start_game_timer ()
{
  finish_millis = millis () + MATCH_DURATION_MS;
}

bool is_game_finished ()
{
  return millis () > finish_millis;
}


void draw_score ()
{
#define N_DIGITS 4
  unsigned char x_base = 24;
  byte img_ram[8];
  for (int i = 0; i < PIXEL_NUMBER_SECTION; i++)
  {
    SSD1306_MINIMAL_framebuffer[i] = 0;
  }

  for (unsigned char current_digit_drawn = 0; current_digit_drawn < N_DIGITS;
       current_digit_drawn++)
  {
    draw_score (img_ram);
    for (unsigned char y = 0; y < 8; y++)
    {

      for (unsigned char x = 0; x < 8; x++)
      {
	int byte_index = y;
	char bit_delta_x = x % 8;

	bool pixel_found = ((B10000000 >> bit_delta_x) & img_ram[byte_index]);

	if (pixel_found)
	{
	  SSD1306_MINIMAL_setPixel (x_base + x, y);
	}
      }
    }
    x_base -= 8;
  }
  SSD1306_MINIMAL_transferFramebuffer ();

  for (int i = 0; i < PIXEL_NUMBER_SECTION; i++)
  {
    SSD1306_MINIMAL_framebuffer[i] = 0;
  }
  SSD1306_MINIMAL_transferFramebuffer ();
  SSD1306_MINIMAL_transferFramebuffer ();
  SSD1306_MINIMAL_transferFramebuffer ();

}
