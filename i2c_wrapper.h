#ifndef I2C_WRAPPER_H
#define I2C_WRAPPER_H

/* I2C wrapper for Arduino MCUs */

//#define ATTINY
#ifdef ATTINY
#include <TinyWireM.h>
#else
#include <Wire.h>
#endif

static inline void I2C_WRAPPER_init ()
{
#ifdef ATTINY
  TinyWireM.begin ();
#else
  Wire.begin ();
#endif
}

static inline void I2C_WRAPPER_beginTransmission (unsigned char address)
{
#ifdef ATTINY
  TinyWireM.beginTransmission (address);
#else
  Wire.beginTransmission (address);
#endif
}

static inline void I2C_WRAPPER_write (unsigned char data)
{
#ifdef ATTINY
  TinyWireM.send (data);
#else
  Wire.write (data);
#endif
}

static inline void I2C_WRAPPER_endTransmission ()
{

  delayMicroseconds (50);
#ifdef ATTINY
  TinyWireM.endTransmission ();
#else
  Wire.endTransmission ();

#endif
  delayMicroseconds (50);
}

#endif
