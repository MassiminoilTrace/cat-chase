static void tone_setup ()
{
  // http://gadgetronicx.com/attiny85-timer-tutorial-generating-time-delay-interrupts/
  pinMode (PIN_BUZZER, OUTPUT);
}

static void tone_start (int ms_duration)
{
  digitalWrite (PIN_BUZZER, HIGH);
  delay (ms_duration);
  digitalWrite (PIN_BUZZER, LOW);
}
